<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<?php
	Yii::app()->clientscript
		->registerCssFile( Yii::app()->theme->baseUrl . '/css/bootstrap.min.css')
		->registerCssFile( Yii::app()->theme->baseUrl . '/css/font-awesome.min.css')
		->registerCssFile( Yii::app()->theme->baseUrl . '/css/ionicons.min.css')
		->registerCssFile( Yii::app()->theme->baseUrl . '/css/AdminLTE.css')
		->registerScriptFile( Yii::app()->theme->baseUrl . '/js/ckeditor/ckeditor.js' )
		->registerScriptFile( Yii::app()->theme->baseUrl . '/js/plugins/bootstrap.min.js',  CClientScript::POS_END )
		->registerScriptFile( Yii::app()->theme->baseUrl . '/js/AdminLTE/app.js',  CClientScript::POS_END )
		
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/hendi_ico.jpeg" type="image/x-icon" />
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <?php include 'header.php' ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <?php include 'sidebar.php'; ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <?php echo $this->judul; ?>
                        <small><?php echo $this->small ?></small>
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">
                    <?php  $this->renderPartial('//site/dialog'); ?>
                    <?php echo $content; ?>   
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
    </body>
</html>