<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<?php $baseurl=Yii::app()->baseUrl; ?>
<ul class="nav navbar-nav">
    <?php if(Yii::app()->user->isRoot){ ?>
    <li>
        <?php echo CHtml::link('Pengguna', array('user/index')) ?>
    </li>
    <?php } ?>
    <?php if(Yii::app()->user->isRoot || Yii::app()->user->isAdmin){ ?>
    <li>
        <?php echo CHtml::link('Kategori', array('kategori/')) ?>
    </li>
    <?php } ?>
    <li>
        <?php echo CHtml::link('Artikel', array('artikel/')) ?>
    </li>
    <li>
        <?php echo CHtml::link('Artikel Favoritku', array('artikelFavorit/')) ?>
    </li>
    <li>
        <?php echo CHtml::link('Forum', array('forum/')) ?>
    </li>
                        
    <!-- User Account: style can be found in dropdown.less -->
    <li class="dropdown user user-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="glyphicon glyphicon-user"></i>
            <span><?php echo Pengguna::namaPengguna(); ?> <i class="caret"></i></span>
        </a>
        <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header bg-light-blue">
                <?php $gbr=  Pengguna::gbrAvatar() ?>
                <?php echo CHtml::image($baseurl.'/img/'.$gbr,Yii::app()->user->name,array('class'=>'img-circle')) ?>
                <p>
                    <?php echo Pengguna::namaPengguna(); ?>
                </p>
                <small><?php echo Yii::app()->user->email; ?></small>
            </li>
                <!-- Menu Body -->
                <li class="user-body">
                    <div class="col-xs-6 text-center">
                        <?php echo CHtml::link('Thread Saya', array('threadSaya/')) ?>
                    </div>
                    <div class="col-xs-6 text-center">
                        <?php echo CHtml::link('Artikel Saya', array('artikelSaya/')) ?>
                    </div>
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                    <div class="pull-left">
                        <?php echo CHtml::link('Profil', array('user/view'),array('class'=>'btn btn-default btn-flat')) ?>
                    </div>
                    <div class="pull-right">
                        <?php echo CHtml::link('Keluar', array('site/logout'),array('class'=>'btn btn-default btn-flat')) ?>
                    </div>
                </li>
        </ul>
    </li>
</ul>