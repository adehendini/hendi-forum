<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<?php $baseurl=Yii::app()->baseUrl; ?>
<aside class="left-side sidebar-offcanvas">                
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <?php $gbr=  Pengguna::gbrAvatar() ?>
                            <?php echo CHtml::image($baseurl.'/img/'.$gbr,Yii::app()->user->name,array('class'=>'img-circle')) ?>
                        </div>
                        <div class="pull-left info">
                            <p>Selamat Datang,<br> <?php echo Pengguna::namaPengguna(); ?></p>
                        </div>
                    </div>
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-file-text-o"></i>
                                <span>Kategori Artikel</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <?php
                                    $artikeles=  Kategori::model()->findAll();
                                    foreach ($artikeles as $artikel) 
                                    {
                                        echo '<li>';
                                        echo CHtml::link($artikel->kategori.'<small class="badge pull-right bg-yellow">'.Kategori::model()->artikel($artikel->id).'</small>', array('artikel/kategori','id'=>$artikel->id));
                                        echo '</li>';
                                    }
                                ?>
                            </ul>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-bullhorn"></i>
                                <span>Kategori Forum</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <?php
                                    $forums=  Kategori::model()->findAll();
                                    foreach ($forums as $forum) 
                                    {
                                        echo '<li>';
                                        echo CHtml::link($forum->kategori.'<small class="badge pull-right bg-yellow">'.Kategori::model()->forum($artikel->id).'</small>', array('forum/kategori','id'=>$forum->id));
                                        echo '</li>';
                                    }
                                ?>
                            </ul>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>