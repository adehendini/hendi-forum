<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<header class="header">
    <?php echo CHtml::link(Yii::app()->name, array('beranda/'), array('class'=>'logo')) ?>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </a>
        <div class="navbar-right">
            <?php include 'topmenu.php';?>            
        </div>
    </nav>
</header>