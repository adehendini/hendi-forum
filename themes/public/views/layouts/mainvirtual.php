<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<?php
	Yii::app()->clientscript
		->registerCssFile( Yii::app()->theme->baseUrl . '/css/bootstrap.min.css')
		->registerCssFile( Yii::app()->theme->baseUrl . '/css/font-awesome.min.css')
		->registerCssFile( Yii::app()->theme->baseUrl . '/css/ionicons.min.css')
		->registerCssFile( Yii::app()->theme->baseUrl . '/css/AdminLTE.css')
		->registerScriptFile( Yii::app()->theme->baseUrl . '/js/plugins/bootstrap.min.js',  CClientScript::POS_END )
		->registerScriptFile( Yii::app()->theme->baseUrl . '/js/AdminLTE/app.js',  CClientScript::POS_END )
		
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="author" content="Ade Hendini(adehendini@gmail.com">
        <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/hendi_ico.jpeg" type="image/x-icon" />
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <?php include 'headerVirtual.php' ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <br>
            <div class="container-fluid">
                <?php  $this->renderPartial('//site/dialog'); ?>
                <?php echo $content; ?>  
            </div>
        </div><!-- ./wrapper -->
    </body>
</html>