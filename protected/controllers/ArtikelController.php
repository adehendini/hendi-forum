<?php
/*
 * Dibuat oleh : Ade Hendini
 * adehendini@gmail.com
 */
class ArtikelController extends GxController {

        public $judul="Artikel";

        public function filters() {
                return array(
                                'accessControl', 
                                'postOnly + delete',
                                );
        }

        public function accessRules() {
                $arr=array('index','view','kategori');

                return array(
                                array('allow', 
                                        'actions'=>$arr,
                                        'expression'=>'Yii::app()->user->isRoot() OR Yii::app()->user->isAdmin() OR Yii::app()->user->isAnggota()',
                                        ),
                                array('deny', 
                                        'users'=>array('*'),
                                        ),
                                );
        }
        
        public function actionIndex() {
                $criteria=new CDbCriteria();
                if(isset($_POST['string']))
                {
                    $string=$_POST['string'];
                    $criteria->addSearchCondition( 'judul', $string, true, 'OR' );
                    $criteria->addSearchCondition( 'isi', $string, true, 'OR' );
                    $criteria->addSearchCondition( 'status', '1', true, 'AND' );
                }else{
                    $criteria->addSearchCondition( 'status', '1', true, 'AND' );
                }
                
		$dataProvider = new CActiveDataProvider('Artikel',array(
                    'criteria'=>$criteria,
                    'pagination'=>array(
                            'pageSize'=>10,
                    ),
                    'sort'=>array(
                        'defaultOrder'=>'tanggal DESC',
                    )
                ));
                
                $this->render('index', array(
                    'dataProvider' => $dataProvider,
                ));
	}
        
	public function actionKategori($id) {
		$criteria=new CDbCriteria();
                if(isset($_POST['string']))
                {
                    $string=$_POST['string'];
                    $criteria->addSearchCondition( 'judul', $string, true, 'OR' );
                    $criteria->addSearchCondition( 'isi', $string, true, 'OR' );
                    $criteria->addSearchCondition( 'status', '1', true, 'AND' );
                    $criteria->addSearchCondition( 'kategori_id', $id, true, 'AND' );
                }else{
                    $criteria->addSearchCondition( 'status', '1', true, 'AND' );
                    $criteria->addSearchCondition( 'kategori_id', $id, true, 'AND' );
                }
                
		$dataProvider = new CActiveDataProvider('Artikel',array(
                    'criteria'=>$criteria,
                    'pagination'=>array(
                            'pageSize'=>10,
                    ),
                    'sort'=>array(
                        'defaultOrder'=>'tanggal DESC',
                    )
                ));
                $this->render('index', array(
                    'dataProvider' => $dataProvider,
                ));
	}
        
        public function actionView($id) {
                $model =$this->loadModel($id, 'Artikel');
                $tandai = new MarkArtikel;

                $tandai->artikel_id=$model->id;
                $tandai->user_id=  Yii::app()->user->id;

		if (isset($_POST['MarkArtikel'])) {
			$tandai->setAttributes($_POST['MarkArtikel']);
                        $tandai->artikel_id=$model->id;

			if ($tandai->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
                                        Dialog::message('Berhasil', 'Artikel Berhasil Ditandai');
					$this->redirect(array('view','id'=>$model->id));
			}
		}
                
		$this->render('view', array(
			'model' => $model,
                        'tandai'=>$tandai
		));
	}
        
        public function cekTandai($artikel) {
            $mark = MarkArtikel::model()->findByAttributes(array(
                'artikel_id'=>$artikel,
                'user_id'=>  Yii::app()->user->id
            ));
            if($mark==NULL)
            {
                return FALSE;
            }else{
                return TRUE;
            }
        }
        
}