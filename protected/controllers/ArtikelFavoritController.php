<?php
/*
 * Dibuat oleh : Ade Hendini
 * adehendini@gmail.com
 */
class ArtikelFavoritController extends GxController {

public $judul="Artikel Favoritku";

public function filters() {
	return array(
			'accessControl', 
			'postOnly + delete',
			);
}

public function accessRules() {
        $arr=array('index','delete','view');
        
	return array(
			array('allow', 
				'actions'=>$arr,
				'expression'=>'Yii::app()->user->isRoot() OR Yii::app()->user->isAdmin() OR Yii::app()->user->isAnggota()',
				),
			array('deny', 
				'users'=>array('*'),
				),
			);
}
        public function actionIndex() {
		$model = new ViewArtikelFavorit('search');
		$model->unsetAttributes();

		if (isset($_POST['string']))
			$model->setPencarian($_POST['string']);

		$this->render('index', array(
			'model' => $model,
		));
	}

        
        public function actionView($id) {
            $model = ViewArtikelFavorit::model()->findByAttributes(array('id'=>$id));
		$this->render('view', array(
			'model' => $model,
		));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) 
                {
                    try{
			$this->loadModel($id, 'MarkArtikel')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('index'));
                    }catch(CDbException $e){
                        if($e->getCode()===23000)
                        {
                            throw new CHttpException(400, Yii::t('err', 'Data Ini Tidak Dapat Dihapus'));
                        }else{
                            throw $e;
                        }
                    }
		}else
                    throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

}