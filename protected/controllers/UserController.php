<?php
/*
 * Dibuat oleh : Ade Hendini
 * adehendini@gmail.com
 */
class UserController extends GxController {

        public $judul="User";

public function filters() {
	return array(
			'accessControl', 
			'postOnly + delete',
			);
}

public function accessRules() {
        $root=array('index','delete','ubah','view');
        $arr=array('ubah','view');
        $semua=array('view','ubah');
	return array(
			array('allow', 
				'actions'=>$root,
				'expression'=>'Yii::app()->user->isRoot()',
				),
			array('allow', 
				'actions'=>$arr,
				'expression'=>'Yii::app()->user->isRoot() OR Yii::app()->user->isAdmin()',
				),
			array('allow', 
				'actions'=>$arr,
				'expression'=>'Yii::app()->user->isRoot() OR Yii::app()->user->isAdmin() OR Yii::app()->user->isAnggota()',
				),
			array('deny', 
				'users'=>array('*'),
				),
			);
}
        public function actionIndex() {
                $this->judul="Pengguna";
		$model = new User('search');
		$model->unsetAttributes();

		if (isset($_POST['string']))
			$model->setPencarian($_POST['string']);

		$this->render('index', array(
			'model' => $model,
		));
	}
        
        public function actionView() {
            $this->judul="Profil";
            $id=  Yii::app()->user->id;
            
            //ubah password
                $model = new UserPasswordForm;
                
                if(isset($_POST['ajax']) && $_POST['ajax']==='change_password-form')
                {
                    echo CActiveForm::validate($model);
                    Yii::app()->end();
                }

                if(isset($_POST['UserPasswordForm']))
                {
                    $model->attributes=$_POST['UserPasswordForm'];
                    if($model->validate() && $model->changePassword())
                    {
                        Dialog::message('Berhasil', 'Password Berhasil Diubah');
                        $this->redirect(array('profil/'));
                    }
                }
                
            //ubah gambar profil
                $user= User::model()->findByPk(Yii::app()->user->id);
                if(isset($_POST['User']))
                {
                    $user->setAttributes($_POST['User']);
                    $avatar=NULL;
                    if(strlen(trim(CUploadedFile::getInstance($user, 'avatar')))>0)
                    {
                        $avatar=  CUploadedFile::getInstance($user, 'avatar');
                        $ext_avatar = $avatar->extensionName;
                        $user->avatar="{$user->email}.{$ext_avatar}";
                     }
                     if ($user->save())
                     {
                        if(strlen(trim($avatar))>0)
                        {
                            $avatar->saveAs('img/'.$user->avatar);
                        }
                        Dialog::message('Berhasil', 'Avatar Berhasil Diubah');
			$this->redirect(array('profil/'));
                     }
                }
                
            $this->render('view', array(
                'model' => $this->loadModel($id, 'User'),
                'user'=>$model,
                'avatar'=>$user,
            ));
        }
        

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) 
                {
                    try{
			$this->loadModel($id, 'User')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('index'));
                    }catch(CDbException $e){
                        if($e->getCode()===23000)
                        {
                            throw new CHttpException(400, Yii::t('err', 'Data Ini Tidak Dapat Dihapus'));
                        }else{
                            throw $e;
                        }
                    }
		}else
                    throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}
        
        public function actionUbah() {
            if(Yii::app()->request->isAjaxRequest)
                {
                    Yii::import('bootstrap.components.TbEditableSaver');
                    $es=new TbEditableSaver('User');
                    $es->update();
                }
                else
                    throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }

}