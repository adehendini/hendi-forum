<?php
/*
 * Dibuat oleh : Ade Hendini
 * adehendini@gmail.com
 */
class ForumController extends GxController {

        public $judul="Forum";

        public function filters() {
                return array(
                                'accessControl', 
                                'postOnly + delete',
                                );
        }

        public function accessRules() {
                $arr=array('index','create','thread','view','komentar','upload','uploadKomentar');
                
                return array(
                                array('allow', 
                                        'actions'=>$arr,
                                        'expression'=>'Yii::app()->user->isRoot() OR Yii::app()->user->isAdmin() OR Yii::app()->user->isAnggota()',
                                        ),
                                array('deny', 
                                        'users'=>array('*'),
                                        ),
                                );
        }
        
        public function actionIndex() {
		
		if (isset($_POST['string']))
                {
                    if(empty($_POST['string']))
                    {
                        $model = Kategori::model()->findAll();
                        $this->render('index', array(
                            'model' => $model,
                        ));
                    }else{
                        $string=$_POST['string'];
                        $criteria=new CDbCriteria();
                        $criteria->addSearchCondition( 'judul', $string, true, 'OR' );
                        $criteria->addSearchCondition( 'isi', $string, true, 'OR' );
                        $dataProvider = new CActiveDataProvider('Thread',array(
                            'criteria'=>$criteria,
                            'pagination'=>array(
                                    'pageSize'=>10,
                            ),
                            'sort'=>array(
                                'defaultOrder'=>'tanggal DESC',
                            )
                        ));
                        $this->render('cari', array(
                            'dataProvider' => $dataProvider,
                        ));
                    }
                }else{
                    $model = Kategori::model()->findAll();
                    $this->render('index', array(
			'model' => $model,
                    ));
                }
	}
        
        public function actionThread($id) {
                $kategori = Kategori::model()->findByPk($id);
                $this->small=$kategori->kategori;
                $criteria=new CDbCriteria();
                if(isset($_POST['string']))
                {
                    $string=$_POST['string'];
                    $criteria->addSearchCondition( 'judul', $string, true, 'OR' );
                    $criteria->addSearchCondition( 'isi', $string, true, 'OR' );
                    $criteria->addSearchCondition( 'kategori_id', $id, true, 'AND' );
                }else{
                    $criteria->addSearchCondition( 'kategori_id', $id, true, 'AND' );
                }
                
		$dataProvider = new CActiveDataProvider('Thread',array(
                    'criteria'=>$criteria,
                    'pagination'=>array(
                            'pageSize'=>10,
                    ),
                    'sort'=>array(
                        'defaultOrder'=>'tanggal DESC',
                    )
                ));
                $this->render('thread', array(
                    'dataProvider' => $dataProvider,
                    'kategori'=>$kategori,
                ));
        }
        
	public function actionCreate($id) {
		$model = new Thread;
                $kategori = Kategori::model()->findByPk($id);
                $this->small=$kategori->kategori;
                $model->kategori_id=$kategori->id;
                $model->user_id=  Yii::app()->user->id;
                $model->tanggal=date('Y-m-d h:i:s');
                
		if (isset($_POST['Thread'])) {
			$model->setAttributes($_POST['Thread']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
                                        Dialog::message('Berhasil', 'Thread Berhasil Disimpan');
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 
                    'model' => $model,
                    'kategori'=>$kategori,
                    ));
	}
        
        public function actionView($id) {
                $model =$this->loadModel($id, 'Thread');
                $this->small = $model->kategori;
                
                $criteria=new CDbCriteria();
                $criteria->addSearchCondition( 'thread_id', $model->id, true, 'AND' );
                $dataProvider = new CActiveDataProvider('Komentar',array(
                    'criteria'=>$criteria,
                    'pagination'=>array(
                            'pageSize'=>3,
                    ),
                    'sort'=>array(
                        'defaultOrder'=>'tanggal ASC',
                    )
                ));
                $pager=$dataProvider->pagination;
                $pager->itemCount=$dataProvider->totalItemCount;
                if(!Yii::app()->request->isAjaxRequest)
                        $pager->currentPage=$pager->pageCount;

                
                //komentari
                $komentar = new Komentar;
                $komentar->thread_id=$model->id;
                $komentar->tanggal=date('Y-m-d h:i:s');
                $komentar->user_id=  Yii::app()->user->id;

                if (isset($_POST['Komentar'])) {
                    $komentar->setAttributes($_POST['Komentar']);

                    if ($komentar->save()) {
                            if (Yii::app()->getRequest()->getIsAjaxRequest())
                                    Yii::app()->end();
                            else
                                Dialog::message('Berhasil', 'Thread Berhasil dikomentari');
                                $this->redirect(array('view','id'=>$model->id));
                            }
                }
                
		$this->render('view', array(
			'model' => $model,
                        'komentar'=>$komentar,
                        'dataProvider'=>$dataProvider,
		));
	}

	public function actionKomentar($id) {
            $thread = Thread::model()->findByPk($id);
            $model = new Komentar;
            $thread = Thread::model()->findByPk($id);
            $model->thread_id=$thread->id;
            $model->tanggal=date('Y-m-d h:i:s');
            $model->user_id=  Yii::app()->user->id;

            if (isset($_POST['Komentar'])) {
		$model->setAttributes($_POST['Komentar']);

		if ($model->save()) {
			if (Yii::app()->getRequest()->getIsAjaxRequest())
				Yii::app()->end();
			else
                            Dialog::message('Berhasil', 'Thread Berhasil dikomentari');
                            $this->redirect(array('forum/view','id'=>$thread->id));
			}
            }

            $this->render('form_komentar', array( 
                        'model' => $model,
                        'thread'=>$thread
                    ));
        }
        
        public function actionUpload()
        {
            if($_FILES) // check if file upload exist
            {
                $target_path = Yii::app()->basePath.'/../gambar/forum/';  // directory in your server
                $ext_name=explode('.',basename($_FILES['upload']['name']));  // get extension from file upload
                $rand1 = rand(11111, 99999);
                $rand2 = rand(1111111111, 9999999999);
                $rand3 = date('Y-m-d');
                $filename= $rand2.'-'.$rand1.'-'.$rand2.'-'.$rand3;
                $fname=$filename.$ext_name[1];  // set your file name, 
                $target_path = $target_path.$fname;  // save path and name file

                if(!file_exists("$target_path"))// if we have set target path
                {
                      $message='';  // message variable, use to show message after upload file
                      $url='';   // url variable, use to set url image in your editor
                      $lower_ext=strtolower($ext_name[1]);  // check extension, validate type file
                      if ($lower_ext!='jpg' && $lower_ext!='jpeg' && $lower_ext!='png' && $lower_ext!='gif')
                      {
                        // if type file not allow
                        $message = "The image must be in either JPG, JPEG, GIF or PNG format."; 
                      }
                    else
                    {
                        // upload file to server
                        move_uploaded_file($_FILES['upload']['tmp_name'], $target_path);
                        echo 'Success Upload File';
                        // url will save directory. Note it, url is different with target path
                        // url will use in your editor, it will direct access to the path
                        $baseurl=  Yii::app()->baseUrl;
                        $url=$baseurl.'/gambar/forum/'.$fname;
                    }
                    $funcNum = $_GET['CKEditorFuncNum'] ;
                    // after save data, run code to call ckeditor function
                    echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
                  }
                  else
                  {
                      echo 'Failed Upload File';    
                  }
            }
        }
        
        public function actionUploadKomentar()
        {
            if($_FILES) // check if file upload exist
            {
                $target_path = Yii::app()->basePath.'/../gambar/komentar/';  // directory in your server
                $ext_name=explode('.',basename($_FILES['upload']['name']));  // get extension from file upload
                $rand1 = rand(11111, 99999);
                $rand2 = rand(1111111111, 9999999999);
                $rand3 = date('Y-m-d');
                $filename= $rand2.'-'.$rand1.'-'.$rand2.'-'.$rand3;
                $fname=$filename.$ext_name[1];  // set your file name, 
                $target_path = $target_path.$fname;  // save path and name file

                if(!file_exists("$target_path"))// if we have set target path
                {
                      $message='';  // message variable, use to show message after upload file
                      $url='';   // url variable, use to set url image in your editor
                      $lower_ext=strtolower($ext_name[1]);  // check extension, validate type file
                      if ($lower_ext!='jpg' && $lower_ext!='jpeg' && $lower_ext!='png' && $lower_ext!='gif')
                      {
                        // if type file not allow
                        $message = "The image must be in either JPG, JPEG, GIF or PNG format."; 
                      }
                    else
                    {
                        // upload file to server
                        move_uploaded_file($_FILES['upload']['tmp_name'], $target_path);
                        echo 'Success Upload File';
                        // url will save directory. Note it, url is different with target path
                        // url will use in your editor, it will direct access to the path
                        $baseurl=  Yii::app()->baseUrl;
                        $url=$baseurl.'/gambar/komentar/'.$fname;
                    }
                    $funcNum = $_GET['CKEditorFuncNum'] ;
                    // after save data, run code to call ckeditor function
                    echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
                  }
                  else
                  {
                      echo 'Failed Upload File';    
                  }
            }
        }

}