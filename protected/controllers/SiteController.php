<?php
/*
 * Dibuat oleh : Ade Hendini
 * adehendini@gmail.com
 */
class SiteController extends GxController
{
	public function actionIndex()
	{
            if(!Yii::app()->user->isGuest)
            {
                $this->layout='//layouts/main';
                $this->judul="Beranda";
		$this->render('index');
            }else{
                $this->login();
            }
	}
        
        public function login()
	{
            $this->layout='//layouts/mainvirtual';
            $model=new LoginForm;
            $user=new User;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
                                $this->redirect(array('index'));
				//$this->redirect('portaladmin/welcome');
		}
                
                if (isset($_POST['User'])) {
                    $user->setAttributes($_POST['User']);
                    $user->avatar='hendi.png';
                    $user->status=1;
                    $user->role=2;

                    if ($user->save()) {
                        if (Yii::app()->getRequest()->getIsAjaxRequest())
                        {
                            Yii::app()->end();
                        }else{
                            Dialog::message('Berhasil', 'Pendaftaran Berhasil Diproses, Silahkan login');
                            $this->redirect(array('site/index'));
                        }
                    }
                }
		// display the login form
            $this->render('login',array(
                'model'=>$model,
                'user'=>$user,
                    ));
	}

	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}