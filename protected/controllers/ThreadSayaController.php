<?php
/*
 * Dibuat oleh : Ade Hendini
 * adehendini@gmail.com
 */
class ThreadSayaController extends GxController {

        public $judul="Thread Saya";
    
        public function filters() {
                return array(
                                'accessControl', 
                                'postOnly + delete',
                                );
        }

        public function accessRules() {
                $arr=array('index');
                
                return array(
                                array('allow', 
                                        'actions'=>$arr,
                                        'expression'=>'Yii::app()->user->isRoot() OR Yii::app()->user->isAdmin() OR Yii::app()->user->isAnggota()',
                                        ),
                                array('deny', 
                                        'users'=>array('*'),
                                        ),
                                );
        }

	public function actionIndex() {
                $criteria=new CDbCriteria();
                if(isset($_POST['string']))
                {
                    $string=$_POST['string'];
                    $criteria->addSearchCondition( 'judul', $string, true, 'OR' );
                    $criteria->addSearchCondition( 'isi', $string, true, 'OR' );
                    $criteria->addSearchCondition( 'user_id', Yii::app()->user->id, true, 'AND' );
                }else{
                    $criteria->addSearchCondition( 'user_id', Yii::app()->user->id, true, 'AND' );
                }
                
		$dataProvider = new CActiveDataProvider('Thread',array(
                    'criteria'=>$criteria,
                    'pagination'=>array(
                            'pageSize'=>10,
                    ),
                    'sort'=>array(
                        'defaultOrder'=>'tanggal DESC',
                    )
                ));
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	

}