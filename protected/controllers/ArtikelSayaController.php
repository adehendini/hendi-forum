<?php
/*
 * Dibuat oleh : Ade Hendini
 * adehendini@gmail.com
 */
class ArtikelSayaController extends GxController {

public $judul="Artikel Saya";

public function filters() {
	return array(
			'accessControl', 
			'postOnly + delete',
			);
}

public function accessRules() {
        $arr=array('index','create','delete','update','view','ubah','upload');
        
	return array(
			array('allow', 
				'actions'=>$arr,
				'expression'=>'Yii::app()->user->isRoot() OR Yii::app()->user->isAdmin() OR Yii::app()->user->isAnggota()',
				),
			array('deny', 
				'users'=>array('*'),
				),
			);
}
        public function actionIndex() {
		$model = new Artikel('search');
		$model->unsetAttributes();

		if (isset($_POST['string']))
			$model->setPencarian($_POST['string']);

		$this->render('index', array(
			'model' => $model,
		));
	}
        
	public function actionCreate() {
		$model = new Artikel;
                $model->tanggal=  date("Y-m-d h:i:s");
                $model->user_id=  Yii::app()->user->id;

		if (isset($_POST['Artikel'])) {
			$model->setAttributes($_POST['Artikel']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
                                        Dialog::message('Berhasil', 'Artikel Berhasil Disimpan');
					$this->redirect(array('artikelSaya/'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Artikel');
                //$model->tanggal=  date("Y-m-d h:i:s");

		if (isset($_POST['Artikel'])) {
			$model->setAttributes($_POST['Artikel']);

			if ($model->save()) {
                                Dialog::message('Berhasil', 'Data Berhasil Diubah');
				$this->redirect(array('update', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}
        
        public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Artikel'),
		));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) 
                {
                    try{
			$this->loadModel($id, 'Artikel')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('index'));
                    }catch(CDbException $e){
                        if($e->getCode()===23000)
                        {
                            throw new CHttpException(400, Yii::t('err', 'Data Ini Tidak Dapat Dihapus'));
                        }else{
                            throw $e;
                        }
                    }
		}else
                    throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

        public function actionUbah() {
            if(Yii::app()->request->isAjaxRequest)
                {
                    Yii::import('bootstrap.components.TbEditableSaver');
                    $es=new TbEditableSaver('Artikel');
                    $es->update();
                }
                else
                    throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }
        
        public function actionUpload()
        {
            if($_FILES) // check if file upload exist
            {
                $target_path = Yii::app()->basePath.'/../gambar/artikel/';  // directory in your server
                $ext_name=explode('.',basename($_FILES['upload']['name']));  // get extension from file upload
                $rand1 = rand(11111, 99999);
                $rand2 = rand(1111111111, 9999999999);
                $rand3 = date('Y-m-d');
                $filename= $rand1.'-'.$rand2.'-'.$rand3;
                $fname=$filename.$ext_name[1];  // set your file name, 
                $target_path = $target_path.$fname;  // save path and name file

                if(!file_exists("$target_path"))// if we have set target path
                {
                      $message='';  // message variable, use to show message after upload file
                      $url='';   // url variable, use to set url image in your editor
                      $lower_ext=strtolower($ext_name[1]);  // check extension, validate type file
                      if ($lower_ext!='jpg' && $lower_ext!='jpeg' && $lower_ext!='png' && $lower_ext!='gif')
                      {
                        // if type file not allow
                        $message = "The image must be in either JPG, JPEG, GIF or PNG format."; 
                      }
                    else
                    {
                        // upload file to server
                        move_uploaded_file($_FILES['upload']['tmp_name'], $target_path);
                        echo 'Success Upload File';
                        // url will save directory. Note it, url is different with target path
                        // url will use in your editor, it will direct access to the path
                        $baseurl=  Yii::app()->baseUrl;
                        $url=$baseurl.'/gambar/artikel/'.$fname;
                    }
                    $funcNum = $_GET['CKEditorFuncNum'] ;
                    // after save data, run code to call ckeditor function
                    echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
                  }
                  else
                  {
                      echo 'Failed Upload File';    
                  }
            }
        }
}