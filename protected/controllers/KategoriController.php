<?php
/*
 * Dibuat oleh : Ade Hendini
 * adehendini@gmail.com
 */
class KategoriController extends GxController {

        public $judul="Kategori";
        

        public function filters() {
                return array(
                                'accessControl', 
                                'postOnly + delete',
                                );
        }

        public function accessRules() {
                $arr=array('index','create','delete','update');

                return array(
                                array('allow', 
                                        'actions'=>$arr,
                                        'expression'=>'Yii::app()->user->isRoot() OR Yii::app()->user->isAdmin()',
                                        ),
                                array('deny', 
                                        'users'=>array('*'),
                                        ),
                                );
        }
        
        public function actionIndex() {
		$model = new Kategori('search');
		$model->unsetAttributes();

		if (isset($_POST['string']))
			$model->setPencarian($_POST['string']);

		$this->render('index', array(
			'model' => $model,
		));
	}
        
	public function actionCreate() {
		$model = new Kategori;

		if (isset($_POST['Kategori'])) {
			$model->setAttributes($_POST['Kategori']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
                                        Dialog::message('Berhasil', 'Data Berhasil Disimpan');
					$this->redirect(array('create'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Kategori');


		if (isset($_POST['Kategori'])) {
			$model->setAttributes($_POST['Kategori']);

			if ($model->save()) {
                                Dialog::message('Berhasil', 'Data Berhasil Diubah');
				$this->redirect(array('update', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}
        

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) 
                {
                    try{
			$this->loadModel($id, 'Kategori')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('index'));
                    }catch(CDbException $e){
                        if($e->getCode()===23000)
                        {
                            throw new CHttpException(400, Yii::t('err', 'Data Ini Tidak Dapat Dihapus'));
                        }else{
                            throw $e;
                        }
                    }
		}else
                    throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

}