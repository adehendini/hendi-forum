<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
class AWebUser extends CWebUser{
 
    protected $_model;
 
    public function isRoot(){
        return $this->getState('isRoot');
    }
    
    public function isAdmin(){
        return $this->getState('isAdmin');
    }
    
    public function avatar(){
        return $this->getState('avatar');
    }
    
    public function email(){
        return $this->getState('email');
    }
    
    // Load user model.
    protected function loadUser()
    {
        if ( $this->_model == null ) {
                $this->_model = User::model()->findByPk(Yii::app()->User->id);
        }
        return $this->_model;
    }
    
}
