<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
class Pengguna {
    
    public function gbrAvatar() {
        $model = User::model()->findByPk(Yii::app()->User->id);
        return $model->avatar;
    }
    
    public function namaPengguna() {
        $model = User::model()->findByPk(Yii::app()->User->id);
        return $model->nama;
    }

}