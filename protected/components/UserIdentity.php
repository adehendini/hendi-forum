<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	private $_id;
        private $role;
        private $_name;
         
	public function authenticate()
	{
		$record= User::model()->findByAttributes(array('email'=>$this->username));    
                if($record===null)
                {
                    $this->errorCode=  self::ERROR_USERNAME_INVALID;
                }else if($record->password!==md5($this->password))
                {
                    $this->errorCode=  self::ERROR_PASSWORD_INVALID;
                }else{
                    if($record->status==0)
                    {
                        $this->errorCode=  self::ERROR_UNKNOWN_IDENTITY;
                    }else{
                        $this->_id=$record->id;
                        $this->_name=$record['nama'];
                        $this->setState('role', $record['role']);
                        $this->setState('avatar', $record['avatar']);
                        $this->setState('email', $record['email']);
                        $this->errorCode=  self::ERROR_NONE;
                        if($record['role']==0)
                        {
                            Yii::app()->user->setState('isRoot',true);
                        }else{
                            Yii::app()->user->setState('isRoot',false);
                        }

                        if($record['role']==0 || $record['role']==1)
                        {
                            Yii::app()->user->setState('isAdmin',true);
                        }else{
                            Yii::app()->user->setState('isAdmin',false);
                        }

                        if($record['role']==2)
                        {
                            Yii::app()->user->setState('isAnggota',true);
                        }
                    }
                }
                return !$this->errorCode;
	}
        
        public function getId() {
            return $this->_id;
        }

        public function getName() {
            return $this->_name;
        }
}