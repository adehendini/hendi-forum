<?php

/* 
 * Ade Hendini.
 */

class Konversi {

    public function tanggal($tanggal="now", $format="j M Y",$bahasa="id") {
        $en=array("Sun","Mon","Tue","Wed","Thu","Fri","Sat","Jan","Feb",
            "Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"
            );
        $id=array("Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu",
            "Januari","Pebruari","Maret","April","Mei","Juni","Juli","Agustus","September",
            "Oktober","Nopember","Desember"
            );
        return str_replace($en,$$bahasa,date($format,strtotime($tanggal)));
    }
    
    public function uang($nilai, $pecahan) {
        return number_format($nilai, $pecahan,',','.');
    }

}

