<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'id' => 'kategori-form',
	'enableAjaxValidation' => false,
        'type'=>'horizontal',
));
?>
<?php echo $form->errorSummary($model); ?>

		<?php echo $form->textFieldGroup($model,'kategori',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>45)))); ?>


<div class="form-actions">                
<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			//'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Simpan' : 'Simpan Perubahan',
                        'icon'=>'fa fa-save fa-fw',
		)); ?>
    &nbsp;
<?php echo CHtml::link('<span class="glyphicon glyphicon-share-alt"></span> Kembali', array('index'), array('class'=>'btn btn-default')) ?>
</div>    
<?php $this->endWidget(); ?>
