<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<div class="row">
    <div class="col-lg-12">
<?php echo CHtml::link('<span class="glyphicon glyphicon-plus"></span> Tambah', array('create'), array('class'=>'btn btn-default')) ?>
        <div class="pull-right">
            <?php $this->renderPartial('_pencarian'); ?>
        </div>
<?php $this->widget('booster.widgets.TbGridView', array(
	'id' => 'kategori-grid',
	'dataProvider' => $model->pencarian(),
	//'filter' => $model,
        'summaryText'=>'',
	'columns' => array(
//		'id',
		'kategori',
		array(
			'header' => 'Aksi',
                        //'htmlOptions'=>array('width'=>'80'),
			'class' => 'booster.widgets.TbButtonColumn',
                        'template'=>'{update}{delete}',
		),
	),
)); ?>
    </div>
</div>