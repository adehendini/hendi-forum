<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<?php $form = $this->beginWidget('GxActiveForm', array(
	//'action' => Yii::app()->createUrl($this->route),
        //'type'=>'horizontal',
	'method' => 'post',
        'htmlOptions'=>array('class'=>'form-inline')
)); ?>
    <div class="row">
      <div class="col-xs-12">
        <div class="input-group input-group-lg">
            <?php echo CHtml::textField('string', (isset($_POST['string'])) ? $_POST['string'] : '', array('class'=>'form-control','placeholder'=>'Pencarian')) ?>
          <div class="input-group-btn">
            <?php echo CHtml::tag('button',
                array('class' => 'btn btn-default','type'=>'submit'),
                '<i class="glyphicon glyphicon-search"></i> Cari'); ?>
          </div><!-- /btn-group -->
        </div><!-- /input-group -->
      </div><!-- /.col-xs-12 -->
    </div><!-- /.row -->
<?php $this->endWidget(); ?>