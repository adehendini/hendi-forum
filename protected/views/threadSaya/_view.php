<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<div class="panel panel-default">
  <div class="panel-heading">
    <h2 class="panel-title"><?php echo CHtml::link($data->judul, array('forum/view', 'id' => $data->id)); ?></h2>
  </div>
  <div class="panel-body">
    <?php echo substr(strip_tags($data->isi),0,250); ?>
    <?php echo CHtml::link('Selengkapnya...', array('forum/view', 'id' => $data->id)); ?>  
  </div>
    <div class="panel-footer">
        <div class="row"> 
            <div class="col-lg-12">
        <div class="pull-left">
            <strong>Jumlah Komentar &nbsp;&nbsp;&nbsp;&nbsp;: <?php echo Komentar::model()->jmlKomentar($data->id) ?></strong><br>
            <strong>Komentar Terakhir &nbsp;: <?php echo Konversi::tanggal(Komentar::model()->latestKomentar($data->id),"d M Y") ?></strong>
        </div>
        <div class="pull-right">
            <strong>Kategori &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?php echo $data->kategori ?></strong><br>
            <strong>Tanggal Post &nbsp;&nbsp;: <?php echo Konversi::tanggal($data->tanggal,"d M Y") ?></strong>
        </div>
            </div>
        </div>
    </div>  
</div>
