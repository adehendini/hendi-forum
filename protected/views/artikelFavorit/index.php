<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<?php
/*
$this->breadcrumbs = array(
	MarkArtikel::label(2),
	Yii::t('app', 'Index'),
);
*/
?>
<div class="row">
    <div class="col-lg-12">
       <?php $this->renderPartial('_pencarian'); ?>

<?php $this->widget('booster.widgets.TbGridView', array(
	'id' => 'mark-artikel-grid',
	'dataProvider' => $model->pencarian(Yii::app()->user->id),
	//'filter' => $model,
        'summaryText'=>'',
	'columns' => array(
                'judul',
                'nama',
                array(
                    'name'=>'tanggal',
                    'value'=>'Konversi::tanggal($data->tanggal,"d M Y")',
                ),
		array(
			'header' => 'Aksi',
                        //'htmlOptions'=>array('width'=>'80'),
			'class' => 'booster.widgets.TbButtonColumn',
                        'template'=>'{view} {delete}',
                        'buttons'=>array(
                            'view'=>array(
                                'url'=>'Yii::app()->createUrl("artikelFavorit/view",array("id"=>$data->id))'
                            ),
                            'delete'=>array(
                                'url'=>'Yii::app()->createUrl("artikelFavorit/delete",array("id"=>$data->id))'
                            ),
                        )
		),
	),
)); ?>
    </div>
</div>