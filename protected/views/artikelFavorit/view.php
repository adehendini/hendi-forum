<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<div class="row">
    <div class="col-lg-12">
<?php echo CHtml::link('<span class="glyphicon glyphicon-share-alt"></span> Kembali', array('index'), array('class'=>'btn btn-default')) ?>
<br><br>        
<h3><?php echo $model->judul; ?></h3>
<hr>
<?php echo $model->isi; ?>
<hr>
<strong>Kategori &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?php echo $model->kategori ?></strong><br>
<strong>Penulis &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?php echo $model->nama ?></strong><br>
<strong>Tanggal dibuat : <?php echo Konversi::tanggal($model->tanggal,"d M Y") ?></strong><br>

    </div>
</div>