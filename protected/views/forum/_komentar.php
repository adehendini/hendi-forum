<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<div class="panel panel-default">
  <div class="panel-body">
    <?php echo $data->isi; ?>
  </div>
    <div class="panel-footer">
        <div class="row"> 
            <div class="col-lg-12">
        <div class="pull-left">
            <strong>Komentator : <?php echo $data->user->nama ?></strong><br>
        </div>
        <div class="pull-right">
            <strong>Tanggal komentar &nbsp;&nbsp;: <?php echo Konversi::tanggal($data->tanggal,"d M Y") ?></strong>
        </div>
            </div>
        </div>
    </div>  
</div>
