<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'id' => 'komentar-form',
	'enableAjaxValidation' => false,
));
?>
<?php echo $form->errorSummary($model); ?>

		<?php    $isi=$this->widget(
                            'booster.widgets.TbCKEditor',
                            array(
                            'model'=>$model,    
                            'name' => 'isi',
                            'attribute'=>'isi',    
                            ),true);  
                        echo $form->customFieldGroup($isi, $model ,'isi');?>  
                
<div class="form-actions">                
<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>'Komentari',
		)); ?>
    &nbsp;
</div>    
<?php $this->endWidget(); ?>

<script type="text/javascript">
    CKEDITOR.replace('Komentar[isi]', {
                height: 300,
                //width : 100,
                // this code use for activate image upload in your view, and it will execute the url that you want
                // when run it
                filebrowserImageUploadUrl: '<?php echo Yii::app()->createUrl("forum/uploadKomentar")?>',

                // set toolbar that you want to show
                toolbar : [
                    //{'name' : 'document' , 'items' : ['Source']},
                    ['Cut','Copy','Paste','PasteText', 'PasteFromWord','-','Undo','Redo','Image'],
                    {'name' : 'basicstyles','items' : ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
                    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
                ]
            } );
</script>