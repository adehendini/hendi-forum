<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<div class="row">
    <div class="col-lg-12">
        <?php $this->renderPartial('_pencarian'); ?>
        <br>
        <?php foreach ($model as $baris) : ?>
        <?php $jmlForum = Kategori::model()->forum($baris->id); ?>
        <?php $tglAkhir = Thread::model()->latestPost($baris->id);
            if(empty($tglAkhir))
            {
                $tglAkhir="00-00-0000";
            }else{
                $tglAkhir = Konversi::tanggal($tglAkhir,"d M Y");
            }           
        ?>
        <?php echo CHtml::link("
        <div class='well well-sm'>
            $baris->kategori ($jmlForum)
            <div class='pull-right'>
                Post Terakhir : $tglAkhir
            </div>    
        </div>
        ",array('thread','id'=>$baris->id)); ?>
        <?php endforeach; ?>
    </div>
</div>