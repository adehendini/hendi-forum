<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<div class="row">
    <div class="col-lg-12">
<?php echo CHtml::link('<span class="glyphicon glyphicon-share-alt"></span> Kategori Forum', array('index'), array('class'=>'btn btn-default')) ?>
        
        <h3><?php echo $model->judul; ?></h3>
        <hr>
        <?php echo $model->isi ?>
        <hr>
<strong>Di Post Oleh &nbsp;&nbsp;&nbsp;: <?php echo $model->user->nama ?></strong><br>
<strong>Tanggal post &nbsp;&nbsp;: <?php echo Konversi::tanggal($model->tanggal,"d M Y") ?></strong>
<hr>

<h4>Komentar</h4>
<?php $this->widget('booster.widgets.TbListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_komentar',
)); ?>
<?php
$this->renderPartial('_form_komentar', array('model'=>$komentar));
?>
    </div>
</div>