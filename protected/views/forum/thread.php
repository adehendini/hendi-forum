<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<div class="row">
    <div class="col-lg-12">
      <?php $this->renderPartial('_pencarian'); ?>  
        <br>
<?php echo CHtml::link('<span class="glyphicon glyphicon-share-alt"></span> Kategori Forum', array('index'), array('class'=>'btn btn-default')) ?>
        &nbsp;
<?php echo CHtml::link('<span class="glyphicon glyphicon-question-sign"></span> Tanya', array('create','id'=>$kategori->id), array('class'=>'btn btn-default')) ?>
        
                
<?php $this->widget('booster.widgets.TbListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_thread',
)); ?>
    </div>
</div>