<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'id' => 'thread-form',
	'enableAjaxValidation' => false,
        //'type'=>'horizontal',
        
));
?>
<?php echo $form->errorSummary($model); ?>

		<?php echo $form->textFieldGroup($model,'judul',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>200)))); ?>
		<?php    $isi=$this->widget(
                            'booster.widgets.TbCKEditor',
                            array(
                            'model'=>$model,    
                            'name' => 'isi',
                            'attribute'=>'isi', 
                            ),true);  
                        echo $form->customFieldGroup($isi, $model ,'isi');
                  ?>    

<div class="form-actions">                
<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			//'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Simpan' : 'Simpan Perubahan',
                        'icon'=>'fa fa-save fa-fw',
		)); ?>
    &nbsp;
<?php echo CHtml::link('<span class="glyphicon glyphicon-share-alt"></span> Batal', array('index'), array('class'=>'btn btn-default')) ?>
</div>    
<?php $this->endWidget(); ?>

<script type="text/javascript">
    CKEDITOR.replace('Thread[isi]', {
                height: 300,
                //width : 100,
                // this code use for activate image upload in your view, and it will execute the url that you want
                // when run it
                filebrowserImageUploadUrl: '<?php echo Yii::app()->createUrl("forum/upload")?>',

                // set toolbar that you want to show
                toolbar : [
                    //{'name' : 'document' , 'items' : ['Source']},
                    ['Cut','Copy','Paste','PasteText', 'PasteFromWord','-','Undo','Redo','Image'],
                    {'name' : 'basicstyles','items' : ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
                    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
                ]
            } );
</script>
