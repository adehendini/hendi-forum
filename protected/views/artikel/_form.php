<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'id' => 'mark-artikel-form',
	'enableAjaxValidation' => false,
));
?>
<?php echo $form->hiddenField($model ,'artikel_id'); ?>

<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			//'context'=>'primary',
			'label'=>'Tandai',
                        'icon'=>'glyphicon glyphicon-star',
		)); ?>
    &nbsp;
<?php $this->endWidget(); ?>
