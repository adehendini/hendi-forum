<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="pull-left">
<?php echo CHtml::link('<span class="glyphicon glyphicon-share-alt"></span> Kembali', array('index'), array('class'=>'btn btn-default')) ?>            
        </div>
        <div class="pull-right">
<?php 
    if($this->cekTandai($model->id)==FALSE)
    {
        $this->renderPartial('_form',array('model'=>$tandai)); 
    }else{
        echo CHtml::link('<span class="glyphicon glyphicon-star"></span> Sudah ditandai',"#", array("submit"=>array('delete', 'id'=>$model->id),'class'=>'btn btn-info'));         
        //echo CHtml::link('<span class="glyphicon glyphicon-star"></span> Sudah ditandai', array('#'), array('class'=>'btn btn-info'));
    }
?>             
        </div>
       
<br><br>        

<h3><?php echo $model->judul; ?></h3>
<hr>
<?php echo $model->isi; ?>
<hr>
<strong>Kategori &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?php echo $model->kategori ?></strong><br>
<strong>Tanggal dibuat : <?php echo Konversi::tanggal($model->tanggal,"d M Y") ?></strong>
    </div>
</div>