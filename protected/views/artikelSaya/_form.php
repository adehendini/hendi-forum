<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'id' => 'artikel-form',
	'enableAjaxValidation' => false,
        //'type'=>'horizontal',
));
?>
<?php echo $form->errorSummary($model); ?>

		<?php $select_kategori_id=$this->widget('booster.widgets.TbSelect2',array(
                                    'model' => $model,
                                    'attribute' => 'kategori_id',
                                    'value' => $model->kategori_id, 
                                    'data' => GxHtml::listDataEx(Kategori::model()->findAllAttributes(null, true)),      
                                    'htmlOptions' => array('class'=>'form-control'),    
                                    'options' => array(
                                        'placeholder' => '--Pilih--',
										'allowClear'=>true,
                                    )),true);

                             echo $form->customFieldGroup($select_kategori_id, $model ,'kategori_id'); ?>
		<?php echo $form->textFieldGroup($model,'judul',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>200)))); ?>

                <?php    $isi=$this->widget(
                            'booster.widgets.TbCKEditor',
                            array(
                            'model'=>$model,    
                            'name' => 'isi',
                            'attribute'=>'isi', 
                            ),true);  
                        echo $form->customFieldGroup($isi, $model ,'isi');
                  ?>   
                 
       
		<?php // echo $form->textAreaGroup($model,'isi', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>6, 'cols'=>50, 'class'=>'span8')))); ?>
		<?php echo $form->dropdownListGroup($model,'status',array('widgetOptions'=>array('data'=>  Artikel::model()->cboStatus()))); ?>


<div class="form-actions">                
<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			//'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Simpan' : 'Simpan Perubahan',
                        'icon'=>'fa fa-save fa-fw',
		)); ?>
    &nbsp;
<?php echo CHtml::link('<span class="glyphicon glyphicon-share-alt"></span> Kembali', array('index'), array('class'=>'btn btn-default')) ?>
</div>    
<?php $this->endWidget(); ?>

<script type="text/javascript">
    CKEDITOR.replace('Artikel[isi]', {
                height: 500,
                //width : 100,
                // this code use for activate image upload in your view, and it will execute the url that you want
                // when run it
                filebrowserImageUploadUrl: '<?php echo Yii::app()->createUrl("artikelSaya/upload")?>',

                // set toolbar that you want to show
                toolbar : [
                    //{'name' : 'document' , 'items' : ['Source']},
                    ['Cut','Copy','Paste','PasteText', 'PasteFromWord','-','Undo','Redo','Image'],
                    {'name' : 'basicstyles','items' : ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
                    { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
                ]
            } );
</script>