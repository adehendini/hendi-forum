<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<div class="row">
    <div class="col-lg-12">
<?php echo CHtml::link('<span class="glyphicon glyphicon-plus"></span> Tambah', array('create'), array('class'=>'btn btn-default')) ?>
    <div class="pull-right">
       <?php $this->renderPartial('_pencarian'); ?>
    </div>
<?php $this->widget('booster.widgets.TbGridView', array(
	'id' => 'artikel-grid',
	'dataProvider' => $model->artikelSaya(),
	//'filter' => $model,
        'summaryText'=>'',
	'columns' => array(
//		'id',
		array(
				'name'=>'kategori_id',
				'value'=>'GxHtml::valueEx($data->kategori)',
				'filter'=>GxHtml::listDataEx(Kategori::model()->findAllAttributes(null, true)),
				),
		'judul',
		array(
                        'class' => 'booster.widgets.TbEditableColumn',
			'name' => 'status',
			'value' => '($data->status == 0) ? Yii::t(\'app\', \'Draf\') : Yii::t(\'app\', \'Publish\')',
			'editable' => array(
                            'type' => 'select',
                            'url' => $this->createUrl('artikelSaya/ubah'),
                            'source' => array(0=>'Draft',1=>'Publish'),
                        )
		),
                array(
                    'name'=>'tanggal',
                    'value'=>'Konversi::tanggal($data->tanggal,"d M Y")'
                ),
		array(
			'header' => 'Aksi',
                        'htmlOptions'=>array('width'=>'80'),
			'class' => 'booster.widgets.TbButtonColumn',
		),
	),
)); ?>
    </div>
</div>