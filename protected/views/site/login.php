<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<div class="row">
    <div class="col-md-6">
        <?php $this->renderPartial('_form_pendaftaran',array('model'=>$user)); ?>
    </div>
    <div class="col-md-6">
        <?php $this->renderPartial('_form_login',array('model'=>$model)); ?>
    </div>
</div>
