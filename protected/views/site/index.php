<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<h1>Selamat Datang di <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>

<div class="row">
    <div class="col-lg-1">
        <?php echo CHtml::image('img/ade_hendini.JPG', 'Ade Hendini',array('class'=>'img-responsive')) ?>
    </div>
    <div class="col-lg-11">
        Aplikasi ini merupakan aplikasi web forum sederhana yang disertai dengan artikel. <br>
        Dibuat oleh saya sendiri <strong>Ade Hendini</strong> dengan menggunakan Yii PHP Framework<br>
        Mudah-mudahan dapat bermanfaat bagi siapapun.
    </div>
</div>
