<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<div class="box box-solid box-success">
    <div class="box-header">
        <h3 class="box-title">Login</h3>
    </div>
    <div class="box-body">
        <?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
            'id'=>'login-form',
            'enableAjaxValidation' => false,
            'enableClientValidation'=>true,
            'clientOptions'=>array(
                'validateOnSubmit'=>true,
            ),
            'type'=>'horizontal',
        ));
        ?>
        <?php echo $form->textFieldGroup($model,'username'); ?>
        <?php echo $form->passwordFieldGroup($model,'password'); ?>
        <?php echo $form->checkboxGroup($model,'rememberMe'); ?>
        <div class="form-actions">                
        <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'context'=>'primary',
                    'label'=>'Login',
                )); ?>
            &nbsp;
        </div> 
        

        <?php $this->endWidget(); ?>
    </div>
</div>


