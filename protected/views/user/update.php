<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<div class="row">
    <div class="col-lg-6">
<?php echo CHtml::link('<span class="glyphicon glyphicon-share-alt"></span> Kembali', array('index'), array('class'=>'btn btn-default')) ?>
<br><br>
<?php
$this->renderPartial('_form', array(
		'model' => $model));
?>
<?php echo Yii::t('app', 'Kolom dengan tanda'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'harus diisi'); ?>.
    </div>
</div>