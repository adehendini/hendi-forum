<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<div class="box box-solid box-info">
    <div class="box-header">
        <h3 class="box-title">Ubah Password</h3>
    </div>
    <div class="box-body">
<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
    'id' => 'change_password-form',
    'enableAjaxValidation' => false,
    'type'=>'horizontal',
    //'htmlOptions'=>array('class'=>'well'),
));
?>
        <?php // echo $form->textFieldGroup($model,'username',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>30)))); ?>
        <?php echo $form->passwordFieldGroup($model,'currentPassword',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>100)))); ?>
        <?php echo $form->passwordFieldGroup($model,'newPassword',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>100)))); ?>
        <?php echo $form->passwordFieldGroup($model,'newPassword_repeat',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>100)))); ?>
        
<div class="form-actions">                
<?php $this->widget('booster.widgets.TbButton', array(
            'buttonType'=>'submit',
            'context'=>'primary',
            'label'=>'Simpan Perubahan',
        )); ?>
</div>    
<?php $this->endWidget(); ?>
    </div>
</div>
    