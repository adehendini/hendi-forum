<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<?php $baseurl=Yii::app()->baseUrl; ?>
<div class="row">
    <div class="col-lg-2">
        <?php echo CHtml::image($baseurl.'/img/'.$model->avatar, $model->nama,array('width'=>'100%')); ?>
        <?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
            'id' => 'user-form',
            'enableAjaxValidation' => false,
            //'type'=>'horizontal',
            'htmlOptions'=>array('enctype'=>'multipart/form-data'),
        ));
        ?>
        <?php echo $form->fileField($avatar,'avatar'); ?>
        <?php echo $form->error($avatar,'avatar'); ?>
        <div class="form-actions">                
        <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    //'context'=>'primary',
                    'label'=>'Upload Avatar',
                    'icon'=>'glyphicon glyphicon-floppy-disk',
                )); ?>
        </div>    
        <?php $this->endWidget(); ?>
    </div>
    <div class="col-lg-10">
        <table class="table">
            <tr>
                <th>Email</th>
                <td><?php echo $model->email; ?></td>
            </tr>
            <tr>
                <th>Nama</th>
                <td><?php
                        $this->widget('booster.widgets.TbEditableField',
                            array(
                                'type' => 'text',
                                'model' => $model,
                                'attribute' => 'nama', // $model->name will be editable
                                'url' => $this->createUrl('user/ubah'), //url for submit data
                            )
                        );
                    ?>
                </td>
            </tr>
            <tr>
                <th>Status</th>
                <td><?php echo $model->status==1 ? "Aktif":"Non Aktif"; ?></td>
            </tr>
            <tr>
                <th>Role</th>
                <td><?php echo User::model()->dataRole($model->role);  ?></td>
            </tr>
        </table>
        
    </div>
</div>
<br>
<div class="row">
    <div class="col-lg-12">
        <?php $this->renderPartial('_change_password',array('model'=>$user)); ?>
    </div>
</div>