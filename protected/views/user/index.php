<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="pull-right">
            <?php $this->renderPartial('_pencarian'); ?>
        </div>
<?php $this->widget('booster.widgets.TbGridView', array(
	'id' => 'user-grid',
	'dataProvider' => $model->pencarian(),
	//'filter' => $model,
        'summaryText'=>'',
	'columns' => array(
//		'id',
		'email',
		'nama',
		array(
                    'class' => 'booster.widgets.TbEditableColumn',
                    'name'=>'status',
                    'editable' => array(
                        'type' => 'select',
                        //'value'=>'$data->status==1 ? "Aktif":"Non Aktif"',
                        'url' => $this->createUrl('user/ubah'),
                        'source' => array(1=>'Aktif',0=>'NonAktif'),
                )),
		array(
                    'class' => 'booster.widgets.TbEditableColumn',
                    'name'=>'role',
                    'editable' => array(
                        'type' => 'select',
                        //'value'=>'$data->status==1 ? "Aktif":"Non Aktif"',
                        'url' => $this->createUrl('user/ubah'),
                        'source' => array(1=>'Admin',2=>'Anggota'),
                )),
		array(
			'header' => 'Aksi',
                        //'htmlOptions'=>array('width'=>'80'),
			'class' => 'booster.widgets.TbButtonColumn',
                        'template'=>'{delete}',
		),
	),
)); ?>
    </div>
</div>