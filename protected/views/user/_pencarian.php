<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<?php echo CHtml::beginForm(CHtml::normalizeUrl(array('index')), 'post', array('class'=>'form-inline','style'=>'width:100%')) ?>
<?php echo CHtml::textField('string', (isset($_POST['string'])) ? $_POST['string'] : '', array('class'=>'form-control col-xs-4','placeholder'=>'Email/Nama')) ?>
<?php echo CHtml::tag('button',
                array('class' => 'btn btn-default','type'=>'submit'),
                '<i class="glyphicon glyphicon-search"></i> Cari'); ?>
<?php echo CHtml::endForm(); ?>
