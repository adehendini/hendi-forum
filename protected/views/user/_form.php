<?php
/*
 * Oleh : Ade Hendini
 * adehendini@gmail.com
 */
?>
<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'id' => 'user-form',
	'enableAjaxValidation' => false,
        'type'=>'horizontal',
));
?>
<?php echo $form->errorSummary($model); ?>

		<?php echo $form->textFieldGroup($model,'email',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>100)))); ?>
		<?php echo $form->textFieldGroup($model,'nama',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>50)))); ?>
		<?php echo $form->passwordFieldGroup($model,'password',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>100)))); ?>
		<?php echo $form->textFieldGroup($model,'avatar',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>100)))); ?>
		<?php echo $form->textFieldGroup($model, 'status'); ?>
		<?php echo $form->textFieldGroup($model, 'role'); ?>



<div class="form-actions">                
<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			//'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Simpan' : 'Simpan Perubahan',
                        'icon'=>'fa fa-save fa-fw',
		)); ?>
    &nbsp;
<?php echo CHtml::link('<span class="glyphicon glyphicon-share-alt"></span> Kembali', array('index'), array('class'=>'btn btn-default')) ?>
</div>    
<?php $this->endWidget(); ?>
