<?php

Yii::import('application.models._base.BaseKategori');

class Kategori extends BaseKategori
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        public function artikel($id) {
            $artikel=  Artikel::model()->findAllByAttributes(array('kategori_id'=>$id));
            $count=  count($artikel);
            return $count;
        }
        
        public function forum($id) {
            $thread= Thread::model()->findAllByAttributes(array('kategori_id'=>$id));
            $count=  count($thread);
            return $count;
        }
}