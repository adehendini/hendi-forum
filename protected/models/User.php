<?php

Yii::import('application.models._base.BaseUser');

class User extends BaseUser
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        public function afterValidate() {
            $this->password=  md5($this->password);
        }
        
        public function dataROle($role) {
            $v="";
            if($role==0)
            {
                $v="Root";
            }
            if($role==1)
            {
                $v="Admin";
            }
            if($role==1)
            {
                $v="Anggota";
            }
            return $v;
        }
}