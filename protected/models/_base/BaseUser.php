<?php

/**
 * This is the model base class for the table "user".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "User".
 *
 * Columns in table "user" available as properties of the model,
 * followed by relations of table "user" available as properties of the model.
 *
 * @property string $id
 * @property string $email
 * @property string $nama
 * @property string $password
 * @property string $avatar
 * @property integer $status
 * @property integer $role
 *
 * @property Artikel[] $artikels
 * @property Komentar[] $komentars
 * @property MarkArtikel[] $markArtikels
 * @property Thread[] $threads
 */
abstract class BaseUser extends GxActiveRecord {

        private $pencarian;

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'user';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'User|Users', $n);
	}

	public static function representingColumn() {
		return 'email';
	}

	public function rules() {
		return array(
			array('email, nama, password, avatar, status, role', 'required'),
                        array('email','email'),
                        array('email','unique'),
                        array('avatar','file','types'=>'jpg,png,jpeg,gif','allowEmpty'=>false,'on'=>'update'),
                        //array('avatar','EImageValidator','types'=>'jpg,png,jpeg,gif','max_size'=>1024,'allowEmpty'=>false,'on'=>'update'),
			array('status, role', 'numerical', 'integerOnly'=>true),
			array('email, password', 'length', 'max'=>100),
			array('nama', 'length', 'max'=>50),
			array('id, email, nama, password, avatar, status, role', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'artikels' => array(self::HAS_MANY, 'Artikel', 'user_id'),
			'komentars' => array(self::HAS_MANY, 'Komentar', 'user_id'),
			'markArtikels' => array(self::HAS_MANY, 'MarkArtikel', 'user_id'),
			'threads' => array(self::HAS_MANY, 'Thread', 'user_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'email' => Yii::t('app', 'Email'),
			'nama' => Yii::t('app', 'Nama'),
			'password' => Yii::t('app', 'Password'),
			'avatar' => Yii::t('app', 'Avatar'),
			'status' => Yii::t('app', 'Status'),
			'role' => Yii::t('app', 'Role'),
			'artikels' => null,
			'komentars' => null,
			'markArtikels' => null,
			'threads' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('nama', $this->nama, true);
		$criteria->compare('password', $this->password, true);
		$criteria->compare('avatar', $this->avatar, true);
		$criteria->compare('status', $this->status);
		$criteria->compare('role', $this->role);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
        
        public function setPencarian($value) {
            $this->pencarian=$value;
        }
        
        public function pencarian() {
		$criteria = new CDbCriteria;
                //$criteria->addSearchCondition('id', $this->pencarian, true,'OR');
		$criteria->addSearchCondition('email', $this->pencarian, true,'OR');
		$criteria->addSearchCondition('nama', $this->pencarian, true,'OR');
		//$criteria->addSearchCondition('password', $this->pencarian, true,'OR');
		//$criteria->addSearchCondition('avatar', $this->pencarian, true,'OR');
		//$criteria->addSearchCondition('status', $this->pencarian, true,'OR');
		//$criteria->addSearchCondition('role', "> 0", true,'OR');
                $criteria->condition = "role > 0";
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}