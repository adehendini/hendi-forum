<?php

Yii::import('application.models._base.BaseThread');

class Thread extends BaseThread
{
        private $_latestPost = null;
        
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        public function latestPost($id) {
            $c = new CDbCriteria();
            //$c->order='tanggal DESC';
            $c->compare('kategori_id', $id);
            
            $thread = Thread::model()->find($c);
            return $thread['tanggal'];
            
        }
}