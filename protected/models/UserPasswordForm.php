<?php

class UserPasswordForm extends CFormModel {

  //  public $username;
    public $currentPassword;
    public $newPassword;
    public $newPassword_repeat;
    private $_user;

    public function rules()
    {
      return array(
        array('currentPassword', 'compareCurrentPassword'),
        array('currentPassword, newPassword, newPassword_repeat', 'required',),
        //array('username','length','min'=>4),  
        array('newPassword,newPassword_repeat','length','min'=>6),  
        array(
          'newPassword_repeat', 'compare',
          'compareAttribute'=>'newPassword',
          'message'=>'Password tidak sama.',
        ),

      );
    }

    public function compareCurrentPassword($attribute,$params)
    {
      if( md5($this->currentPassword) !== $this->_user->password )
      {
        $this->addError($attribute,'Password Lama tidak valid');
      }
    }

    public function init()
    {
      $this->_user = User::model()->findByPk( array( 'id'=>Yii::app()->User->id ) );
    }

    public function attributeLabels()
    {
      return array(
        'currentPassword'=>'Password Lama',
        'newPassword'=>'Password Baru',
        'newPassword_repeat'=>'Konfirmasi Password',
        //'username'=>'Username',  
      );
    }

    public function changePassword()
    {
      $this->_user->password = $this->newPassword;
      //$this->_user->username = $this->username;
      if( $this->_user->save() )
        return true;
      return false;
    }

}