<?php

Yii::import('application.models._base.BaseArtikel');

class Artikel extends BaseArtikel
{
        const DRAFT=0;
        const PUBLISH=1;
        
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        public function cboStatus() {
            return array(
                self::DRAFT=>'Draft',
                self::PUBLISH=>'Publish',
            );
        }
}