<?php

Yii::import('application.models._base.BaseKomentar');

class Komentar extends BaseKomentar
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
        
        public function jmlKomentar($id) {
            $model=  Komentar::model()->findAllByAttributes(array('thread_id'=>$id));
            $komentar = count($model);
            return $komentar;
        }
        
        public function latestKomentar($id) {
            $c = new CDbCriteria();
            //$c->order='tanggal DESC';
            $c->compare('thread_id', $id);
            
            $komentar = Komentar::model()->find($c);
            return $komentar['tanggal'];
            
        }
}